@echo off
set VPN=EdUni
set TASK="Reconnect EdUni"
net session >nul 2>&1
if NOT %errorLevel% == 0 (
  echo Current permissions insufficient. Run script as administrator.
  pause
  exit 1
)
schtasks /Change /TN %TASK% /DISABLE
rasdial "%VPN%" /DISCONNECT
