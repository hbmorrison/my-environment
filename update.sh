#!/bin/sh

# Configuration.

DEBIAN_PACKAGES="bind9-dnsutils curl g++ gcc git-flow gnome-keyring gpg \
  hiera-eyaml inetutils-traceroute lsof make ncat nmap ruby ruby-dev socat vim \
  wget whois xclip zip"
CHOCO="/c/ProgramData/chocolatey/bin/choco"
CHOCO_PACKAGES="7zip ant autohotkey.portable firacode gitkraken kitty maven \
  nmap op openjdk rsync ruby vscodium wincrypt-sshagent winscp"
CHOCO_WINDOWS_10_PACKAGES="microsoft-windows-terminal"
GEMS="ffi hiera-eyaml json rest-client"
SHORTCUTS="wincryptsshagent"
VSCODIUM_EXTENSIONS="mhutchie.git-graph puppet.puppet-vscode vscodevim.vim"
WINDOWS_TERMINAL_SETTINGS_DIR="AppData/Local/Packages/Microsoft.WindowsTerminal_8wekyb3d8bbwe/LocalState"
STARTUP_DIR="${HOME}/AppData/Roaming/Microsoft/Windows/Start Menu/Programs/Startup"

# Figure out which OS and terminal is being used.

case $(cat /proc/version 2>/dev/null) in
  *Chromium\ OS*)
    SHELL_ENVIRONMENT="chromeos"
    VSCODIUM_BIN="/usr/bin"
    VSCODIUM_SETTINGS_DIR="${HOME}/.config/VSCodium/User"
    ;;
  *microsoft-standard-WSL2*)
    SHELL_ENVIRONMENT="wsl"
    ;;
  MSYS*)
    SHELL_ENVIRONMENT="gitbash"
    VSCODIUM_BIN="/c/Program Files/VSCodium/bin/"
    VSCODIUM_SETTINGS_DIR="${HOME}/.config/VSCodium/User"
    ;;
  *)
    echo "Error: Unknown architecture / distribution in /proc/version"
    exit 1
    ;;
esac

# Work out if additional Windows 10 packages are required.

if [ $SHELL_ENVIRONMENT = "gitbash" ]
then
  WINDOWS_VERSION=`powershell '(Get-CimInstance Win32_OperatingSystem).Caption' | cut -d' ' -f3`
  if [ $WINDOWS_VERSION = "10" ]
  then
    CHOCO_PACKAGES="${CHOCO_PACKAGES} ${CHOCO_WINDOWS_10_PACKAGES}"
  fi
fi

# Initialise submodules.

SCRIPT=$(readlink -f $0)
BASE_DIR=$(dirname $SCRIPT)
(cd "$BASE_DIR"; git submodule update --init --recursive)

# Update and install required packages.

case $SHELL_ENVIRONMENT in
  chromeos|wsl)
    sudo apt-get update
    sudo apt-get -y dist-upgrade
    sudo apt-get --no-install-recommends install -y $DEBIAN_PACKAGES
    ;;
  gitbash)
    if [ ! -r $CHOCO ]
    then
      powershell Start-Process -Verb runas -Wait powershell -ArgumentList "\"-NoExit Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))\""
    fi
    TO_BE_INSTALLED=""
    for PACKAGE in $CHOCO_PACKAGES
    do
      echo -n "Checking if $PACKAGE is installed ... "
      INSTALLED=`$CHOCO info -l -r $PACKAGE`
      if [ "${INSTALLED}" = "" ]
      then
        echo "no"
        TO_BE_INSTALLED="${PACKAGE} ${TO_BE_INSTALLED}"
      else
        echo "yes"
      fi
    done
    if [ "${TO_BE_INSTALLED}" != "" ]
    then
      echo "Installing ${TO_BE_INSTALLED}"
      powershell Start-Process -Verb runas -Wait powershell -ArgumentList "\"choco install -y $TO_BE_INSTALLED\""
    fi
    echo "Checking for updates"
    powershell Start-Process -Verb runas -Wait powershell -ArgumentList "\"choco upgrade all -y\""
    ;;
esac

# Install Puppet Agent.

case $SHELL_ENVIRONMENT in
  chromeos)
    sudo apt-get install -y puppet
    ;;
  wsl)
    echo -n "Checking if Puppet Agent is installed ... "
    if [ -r "/opt/puppetlabs/bin/puppet" ]
    then
      echo "yes"
    else
      echo "no"
      echo "Installing Puppet Agent"
      DEBIAN_PUPPET_REPO="https://apt.puppet.com/puppet7-release-${VERSION_CODENAME}.deb"
      PUPPET_REPO_FILE=`basename $DEBIAN_PUPPET_REPO`
      wget $DEBIAN_PUPPET_REPO -O /tmp/$PUPPET_REPO_FILE
      sudo dpkg -i /tmp/$PUPPET_REPO_FILE
      sudo apt-get update
      sudo apt-get install -y puppet-agent
      echo "Stopping and disabling Puppet Agent"
    fi
    ;;
  gitbash)
    echo -n "Checking if Puppet Agent is installed ... "
    INSTALLED=`$CHOCO info -l -r puppet-agent`
    if [ "${INSTALLED}" = "" ]
    then
      echo "no"
      echo "Installing Puppet Agent"
      powershell Start-Process -Verb runas -Wait powershell -ArgumentList "\"choco install -y puppet-agent\""
      echo "Stopping and disabling Puppet Agent"
      powershell Start-Process -Verb runas -Wait powershell -ArgumentList "\"Set-Service -StartupType Disabled puppet; Stop-Service puppet\""
    else
      echo "yes"
    fi
    ;;
esac

# Install VSCodium on ChromeOS.

case $SHELL_ENVIRONMENT in
  chromeos)
    if [ ! -e /usr/share/keyrings/vscodium-archive-keyring.asc ]
    then
      wget -qO- https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg > /tmp/vscodium-archive-keyring.asc
      sudo install -D -o root -g root -m 644 /tmp/vscodium-archive-keyring.asc /usr/share/keyrings/vscodium-archive-keyring.asc
      rm -f /tmp/vscodium-archive-keyring.asc
    fi
    if [ ! -e /etc/apt/sources.list.d/vscodium.list ]
    then
      echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.asc ] https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs vscodium main' \
    | sudo tee /etc/apt/sources.list.d/vscodium.list
      sudo apt update
    fi
    echo -n "Checking if VSCodium is installed ... "
    INSTALLED=`which codium 2>/dev/null`
    if [ "${INSTALLED}" = "" ]
    then
      echo "no"
      echo "Installing VSCodium"
      sudo apt-get install -y codium
    else
      echo "yes"
    fi
    ;;
esac

# Install ruby gems.

INSTALL_GEMS=""
for GEM in $GEMS
do
  echo -n "Checking if $GEM is installed ... "
  gem list --local $GEM | grep ^$GEM > /dev/null 2>&1
  if [ $? -gt 0 ]
  then
    echo "no"
    INSTALL_GEMS="$INSTALL_GEMS $GEM"
  else
    echo "yes"
  fi
done

if [ "$INSTALL_GEMS" != "" ]
then
  case $SHELL_ENVIRONMENT in
    chromeos|wsl)
      sudo gem install $INSTALL_GEMS
      ;;
    gitbash)
      powershell Start-Process -Verb runas -Wait powershell -ArgumentList "\"gem install $INSTALL_GEMS\""
      ;;
  esac
fi

# Set environment variables for VSCodium to use Windows OpenSSH.

case $SHELL_ENVIRONMENT in
  gitbash)
    echo "Setting environment variables for Visual Studio Code"
    PowerShell -Command "[System.Environment]::SetEnvironmentVariable('SSH_AUTH_SOCK','\\\\.\\pipe\\openssh-ssh-agent','User')"
    PowerShell -Command "[System.Environment]::SetEnvironmentVariable('GIT_SSH_COMMAND','C:/Windows/System32/OpenSSH/ssh.exe','User')"
    ;;
esac

# Import registry customisations.

case $SHELL_ENVIRONMENT in
  gitbash)
    KITTY_USERNAME=`PowerShell -Command "Get-ItemPropertyValue -Path 'HKCU:\\SOFTWARE\\9bis.com\\KiTTY\\Sessions\\Solarized%20Dark' -name UserName" 2> /dev/null`
    if [ "${KITTY_USERNAME}" != "isapps" ]
    then
      echo "Setting solarized dark theme in Kitty"
      reg import ${BASE_DIR}/kitty/solarized_dark.reg
      if [ "$DOMAIN" != "ED" ]
      then
        echo "Setting SOCKS5 proxy in Kitty"
        reg import ${BASE_DIR}/kitty/socks5_proxy.reg
      fi
    fi
    if [ $WINDOWS_VERSION = "10" ]
    then
      ACCENT_COLOUR=`PowerShell -Command "Get-ItemPropertyValue -Path 'HKCU:\\SOFTWARE\\Microsoft\\Windows\\DWM' -name AccentColorInactive"`
      if [ "${ACCENT_COLOUR}" = "" ]
      then
        echo "Setting inactive window title bar colour"
        reg import ${BASE_DIR}/windows/titlebar_accent_color.reg
      fi
    fi
    ;;
esac

# Install startup shortcuts on Windows.

case $SHELL_ENVIRONMENT in
  gitbash)
    WINDOWS_BASE_DIR=`cygpath -w $BASE_DIR | sed 's/\\\/\\\\\\\/g'`
    NOT_INSTALLED_SHORTCUTS=""
    for SHORTCUT in $SHORTCUTS
    do
      echo -n "Checking if ${SHORTCUT} shortcut is installed ... "
      if [ ! -f "${STARTUP_DIR}/${SHORTCUT}.lnk" ]
      then
        echo "no"
        NOT_INSTALLED_SHORTCUTS="${NOT_INSTALLED_SHORTCUTS} ${SHORTCUT}"
      else
        echo "yes"
      fi
    done
    for SHORTCUT in $NOT_INSTALLED_SHORTCUTS
    do
      echo "Installing ${SHORTCUT} shortcut"
      powershell "Set-ExecutionPolicy Bypass -Scope Process -Force; ${WINDOWS_BASE_DIR}\\shortcuts\\${SHORTCUT}.ps1"
    done
    ;;
esac

# Configure VSCodium.

if [ "x${VSCODIUM_SETTINGS_DIR}" != "x" ]
then
  mkdir -p "${VSCODIUM_SETTINGS_DIR}"
  rm -f "${VSCODIUM_SETTINGS_DIR}/settings.json" 2> /dev/null
  cp -f "${BASE_DIR}/vscodium/settings.json" "${VSCODIUM_SETTINGS_DIR}/settings.json"
fi

# Configure VSCodium extensions.

NOT_INSTALLED_EXTENSIONS=""
for EXTENSION in $VSCODIUM_EXTENSIONS
do
  echo -n "Checking if VSCodium extension $EXTENSION is installed ... "
  INSTALLED=`"${VSCODIUM_BIN}/codium" --list-extensions 2>/dev/null | grep $EXTENSION 2>/dev/null`
  if [ "${INSTALLED}" = "" ]
  then
    echo "no"
    NOT_INSTALLED_EXTENSIONS="${NOT_INSTALLED_EXTENSIONS} $EXTENSION"
  else
    echo "yes"
  fi
done

for EXTENSION in $NOT_INSTALLED_EXTENSIONS
do
  echo "Installing VSCodium extension $EXTENSION"
  "${VSCODIUM_BIN}/codium" --install-extension $EXTENSION >/dev/null 2>/dev/null
  if [ $? -gt 0 ]
  then
    echo "Error installing VSCodium extension $EXTENSION"
  fi
done

# Configure Vim.

if [ -d $HOME/.vim ]
then
  if [ -d $HOME/.vim.old ]
  then
    rm -rf "${HOME}/.vim.old" 2> /dev/null
  fi
  mv "${HOME}/.vim" "${HOME}/.vim.old" 2> /dev/null
fi

cp -r "${BASE_DIR}/vim" "${HOME}/.vim"
mkdir -p "${HOME}/.vim/autoload"
rm -f "${HOME}/.vim/autoload/pathogen.vim" 2> /dev/null
cp -f "${BASE_DIR}/vim-pathogen/autoload/pathogen.vim" "${HOME}/.vim/autoload/pathogen.vim"

# Configure Windows Terminal.

case $SHELL_ENVIRONMENT in
  gitbash)
    rm -f "${HOME}/${WINDOWS_TERMINAL_SETTINGS_DIR}" 2> /dev/null
    cp -f "${BASE_DIR}/terminal/settings.json" "${HOME}/${WINDOWS_TERMINAL_SETTINGS_DIR}/settings.json"
    ;;
esac
